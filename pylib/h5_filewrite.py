import h5py
def write_h5_dataset(x,filename,compression='gzip',compression_opts=4):
    '''write data set x to filename in h5 format
    '''
    f = h5py.File(filename+".hdf5", "w")
    dset=f.create_dataset('mytestdata',data=x,dtype='float32',compression="gzip", compression_opts=compression_opts)
    f.close()
